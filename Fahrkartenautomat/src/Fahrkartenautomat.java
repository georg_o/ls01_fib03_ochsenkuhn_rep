﻿import java.util.Scanner;

class Fahrkartenautomat
{
	
    public static void main(String[] args)
    {
    	Scanner tastatur = new Scanner(System.in);
    	
    	int x = 1;
    	
    	while (x==1)
    	{
    	
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag = 0.0;
       double zuZahlenderBetrag;
       
       
       zuZahlenderBetrag = fahrkartenbestellungErfassen();
       
       // Geldeinwurf
       // -----------
       rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag) - zuZahlenderBetrag;
       // Fahrscheinausgabe
       // -----------------
       fahrkartenAusgeben();

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rueckgeldAusgeben(rückgabebetrag);
       
       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
       
       System.out.println("\nWollen Sie weitere Fahrscheine kaufen?");
       System.out.println("\nGeben Sie 1 für ja und 2 für nein ein");
       
       int weiter = tastatur.nextInt();
       
       x = weiter;
       
    	}
       
    }
   
    public static double fahrkartenbestellungErfassen() {
    	
    	Scanner tastatur = new Scanner(System.in);
    	
    	double zuZahlenderBetrag =0;
    	int anzahl;
    	
    	double[] Fahrkartenpreise = {2.9, 3.3, 3.6, 1.9, 8.6, 9.0, 9.6, 23.5, 24.3, 24.9};
    	
    	String[] Fahrkartenbezeichnung = {"Einzelfahrschein AB", "Einzelfahrschein BC", "Einzelfahrschein ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", 
    			"Tageskarte Berlin ABC", "Kleingruppen-Tageskarte AB", "Kleingruppen-Tageskarte BC", "Kleingruppen-Tageskarte ABC"};
    	
    	System.out.print("Fahrkartenbestellvorgang:\n=========================\n\n");
    	System.out.print("Wählen Sie ihre Wunschfahrkarte für Berlin aus:\n\n");
    	System.out.printf("%s %18s %40s %n", "Auswahlnummer", "Bezeichnung", "Preis in Euro");
    	for(int i=0; i < Fahrkartenpreise.length; i++)
    	{
    		System.out.printf("%-20d %-30s %20s", i+1, Fahrkartenbezeichnung[i], Fahrkartenpreise[i] + "0  \n");
    	}
    	
    	System.out.print("Ihre Wahl: ");
    	int wahl = tastatur.nextInt();
    	
    	// hier erspart man sich dank des Arrays sehr viel Programmierarbeit
    	zuZahlenderBetrag = Fahrkartenpreise[wahl-1];
    	
    	
        System.out.printf("%s", "Anzahl der Tickets: ");
 	    anzahl = (int) tastatur.nextDouble();
 	    
 	   
 	    return zuZahlenderBetrag * anzahl;
 	    }

    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	
    	Scanner tastatur = new Scanner(System.in);
    	
    	double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("%s %.2f %s\n","Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag)," Euro");
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   double eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        
        return eingezahlterGesamtbetrag; 	
        }
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    	
    }
    public static void rueckgeldAusgeben(double rückgabebetrag) {
    	if(rückgabebetrag > 0.0)
        {
     	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
            }
        }
    }
}