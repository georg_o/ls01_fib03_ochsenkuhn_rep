class Arrays01 {
	public static void main(String[] args) {
		
		double[] temperatur = { 4.0, 3.5, 7.9, 45.5, 4.3, 1.7, 3.8 };
		
		System.out.println(berechneMittelwert(temperatur));
	
	}
	
	//Deklaration der Funktion berechneMittelwert:
	static double berechneMittelwert(double[] array) {
		double mittelwert = 0.0;
		for (int i=0; i< array.length; i++) {
			mittelwert += array[i];
		}
		mittelwert /= array.length;
		return mittelwert;
	}
}