package aufgaben;

import java.util.Scanner;

public class Schleifen5 {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
		System.out.print("Laufzeit (in Jahren) des Sparvertrages:\n");
		int laufzeit = tastatur.nextInt();
		System.out.print("Wie viel Kapital (in Euro) m�chten Sie anlegen:\n");
		double kapital = tastatur.nextDouble();
		System.out.print("Zinssatz:\n");
		double zinssatz = tastatur.nextDouble();
		
		double ein = kapital;
		double zins = zinssatz/100 +1;
		int x = 0;
		double aus = 0;
		while (x < laufzeit)
		{
			kapital *= zins;
			x++;
		}
		System.out.printf("Eingezahltes Kapital: %.2f�\n", ein);
		System.out.printf("Ausgezahltes Kapital: %.2f�\n", kapital);

	}

}
