package aufgaben;

import java.util.Scanner;

public class Schleifen3 {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
		System.out.print("Startwert in Celsius:\n");
		int n = tastatur.nextInt();
		System.out.print("Endwert in Celsius:\n");
		int m = tastatur.nextInt();
		System.out.print("Schrittweite in Celsius\n");
		int s = tastatur.nextInt();
		
		//(0 �C � 9/5) + 32 = 32 �F
		
		int z =0;
		while (n <=m )
		{
			System.out.println(n+"�C");
			z = (n*9/5)+32;
			System.out.println(z+"�F");
			n +=s;
		}

	}

}
